# Gitee::Frontend

gitee-frontend 是一个前端组件库，基于 jQuery 和 Semantic-UI 实现，包含了[码云](https://gitee.com)现在用到的一些通用性较强的组件。

[![giteego npm](https://gitee.com/gitee-frontend/gitee-frontend/badge/giteego.svg?name=npm&id=7796)](https://gitee.com/oschina/dashboard/projects/gitee-frontend/gitee-frontend/giteegos/7796?branch=master)
[![npm](https://img.shields.io/npm/v/gitee-frontend.svg)](https://gitee.com/gitee-frontend/gitee-frontend/releases)
[![LICENSE](https://img.shields.io/npm/l/gitee-frontend.svg)](https://gitee.com/gitee-frontend/gitee-frontend/blob/master/LICENSE)
[![gitee-release-cli](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-gitee--release-blueviolet.svg)](https://gitee.com/gitee-frontend/gitee-release-cli)

## 依赖

以下依赖项需要手动引入。

- [jQuery](https://jquery.com/)
- [Semantic-UI](https://semantic-ui.com/)
- [Date Range Picker](http://www.daterangepicker.com)

## 组件

- **[FilteredSearchBox](#filteredsearchbox):** 筛选搜索框，设计参考自 GitLab 的同名组件，改进了条件多选功能，支持在输入框中选择各种筛选条件，与常规的筛选面板相比，页面空间占用小，操作方便。
- **[Boards](#boards):** 看板，以板块+卡片的形式展示任务的状态和进度。

## 安装

```bash
npm install gitee-frontend --save
```

## 使用

具体用法请参考示例：[https://gitee-frontend.gitee.io/gitee-frontend/](https://gitee-frontend.gitee.io/gitee-frontend/)

**在页面里作为 jQuery 插件使用：**

```html
<link rel="stylesheet" href="dist/jquery/jquery.filtered-search-box.css">
<link rel="stylesheet" href="dist/jquery/jquery.boards.css">
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="dist/jquery/jquery.filtered-search-box.min.js"></script>
<script src="dist/jquery/jquery.boards.min.js"></script>
<script>
var $search = $('#js-search-box');
var $boards = $('#js-boards');

$search.filteredSearchBox({
    // ...
});
$boards.boards({
    // ...
});

var search = $search.data('filteredsearchbox');
var boards = $boards.data('boards')
</script>
```

**作为 ES 模块使用：**

``` js
import '~gitee-frontend/dist/gitee-frontend.css'
import { Boards, FilteredSearchBox } from 'gitee-frontend'

const boards = new Boards($('#js-boards'), { /* options... */ })
const search = new FilteredSearchBox($('#js-search-box'), { /* options... */ })
```

**自定义组件样式：**

```scss
// 自定义组件的主题色
$primary-color: #409eff;

@import "~gitee-frontend/src/variables";
@import "~gitee-frontend/src/components/filtered-search-box.scss";
@import "~gitee-frontend/src/components/boards.scss";

// 覆盖组件样式
.filtered-search-token {
    // ...
}
```

### FilteredSearchBox

示例：

``` js
var options = {
    data: function () {
        return {
            gender: 'male'
        }
    },
    text: {
      loading: '载入中...',
      searchHelp: '按回车键或点击此处搜索',
      selectOtherFilter: '选择其它筛选条件',
      placeholder: '搜索或过滤结果...'
    },
    filters: [
        {
            name: '性别',
            key: 'gender',
            none: {
                name: '不限',
                value: 'none'
            },
            items: [
                {
                    name: '男',
                    value: 'male'
                }, {
                    name: '女',
                    value: 'female'
                }
            ]
        }
    ],
    callback: function (data) {
        console.log('性别是：', data.gender)
    }
}
$('#example-element').filteredSearchBox(options)
```

配置说明：

- **data:** `Function(): Object` 初始数据，值必须是个函数，其返回值类型必须是 `Object`
- **history:** `Object,Boolean` 历史记录，如果想禁用用该功能，可设置为 `false`
  - **limit** `Number` 最大数量，默认值为 5，即：最多保存近 5 条搜索历史
  - **store** `Object` 存储库对象，默认值为 `localStorage`，如需自定义存储位置，请提供包含 `setItem()`、`getItem()`和 `removeItem()` 方法的对象。
  - **storeKey** `String` 存储键值，默认值为 `"GiteeSearchHistory"`
- **filters:** `Array` 筛选器列表
  - **key**: `String` 筛选器标识，例如：`"member_id"`
  - **name:** `String` 筛选器名称，例如：`"成员"`
  - **type:** `String` 筛选器类型，如需日期范围筛选，请指定为`"daterange"`
  - **icon:** `String` 筛选器图标，例如：`"icon user"`
  - **none:** `Object` “无”条件，例如：`{ name: "未关联", value: 0 }`
  - **tabs:** `Object` tab 配置
    - **name:** `String` tab 名称
    - **key:** `String` tab key
    - **active:** `String` 'active' || '' 初始化激活 tab
  - **searchOption:** `Object` 开启远程搜索选项，远程搜索的配置（暂时在 tabs 中使用）
    - **key** 对应的 tab key 值
    - **searchTip** 搜索提示
    - **fetcher** `Function(config, onSuccess, onError)` 获取器，如需自定义请求方式，可指定它
    - **converter** `Function(data): Object` 数据转换器，用于将请求到的数据转换为组件支持的结构
  - **config:** `any` 给筛选器的配置参数，当筛选器类型为 `"daterange"` 时，需要手动指定 [DateRangePicker](http://www.daterangepicker.com) 插件的配置
  - **items:** `Array` 选项列表
    - **name:** `String` 名称
    - **value:** `String` 值
    - **image:** `String` 图片地址，例如：`"https://gitee.com/logo-black.svg"`
    - **icon:** `String` 图标
    - **iconStyle:** `String` 图标样式，例如：`"background-color: #f00;"`
    - **color:** `String` 颜色，例如：`"#f00"`
    - **description:** `String` 附加说明
    - **keywords** `String` 关键词
  - **remote:** `[Object, Array]` 远程选项列表的配置（可选），设置它后，将会请求指定地址来获取选项列表
    - **url:** `String` API 地址
    - **params:** `Object, Function(data): Object`，请求时携带的参数
    - **fetcher:** `Function(config, onSuccess, onError)` 获取器，如需自定义请求方式，可指定它
    - **converter:** `Function(data): Object` 数据转换器，用于将请求到的数据转换为组件支持的结构
- **groups:** `Array` 分组
  - **keys:** `Array` 组内的筛选器的标识列表，例如：`["author_id", "project_id"]`
  - **name:** `String` 组名
- **text:** `Object` 文本翻译
- **callback:** `Function(data)` 确定搜索时的回调，传入参数是当前的筛选参数
- **debug** `Boolean` 是否开启调试

### Boards

用法：

``` js
var options = {
    // 配置
}

$('#example-element').boards(options)
```

配置说明：

- **key:** `String` 用于标识任务所属板块的字段名，默认值为 `"state"`
- **name:** `Sting` 看板名称，默认值为 `"board"`。当页面中存在多个看板时建议设置该属性，以避免内容冲突
- **message:** `Object` 提示信息
  - **loading:** `String` 加载中的提示
  - **stateDisabled:** `String` 当板块不接受某一状态的任务时的提示
  - **allComplete:** `String` 板块内所有任务已经加载完的提示
  - **btnSetFirst:** `String` 板块前置按钮的提示
  - **btnSetLast:** `String` 板块后置按钮的提示
- **className:** `Object` 一些元素的样式类名称
  - **iconComment:** `String` 评论图标
  - **iconIssue:** `String` 任务图标
  - **iconAngleLeft:** `String` 左箭头图标
  - **iconAngleRight:** `String` 右箭头图标
  - **card:** `String` 卡片
  - **avatar:** `String` 头像
  - **action:** `String` 操作按钮
  - **actions:** `String` 操作按钮组
- **data:** `Array` 板块列表
  - **name:** `String` 名称
  - **state:** `String` 状态
  - **color:** `String` 主题色
- **plugins:** `Object` 插件
  - **LetterAvatar:** `Object` 字符头像插件，当传入该插件后，对于未设置头像用户，将显示字符头像。地址：https://gist.github.com/leecrossley/6027780
  - **Sortable:** `Object` 排序插件，当传入该插件后，将支持对板块进行排序。地址：https://github.com/RubaXa/Sortable
- **actions:** `Function(Config)` 板块的操作按钮集的构造函数，返回值类型必须为 `Array`
- **actions:** `Array` 板块的操作按钮集
  - **id:** `String` 标识
  - **class:** `String` 样式类名称
  - **icon:** `String` 图标
  - **title:** `String` 提示文本
  - **callback** `Function(Boards, Board, Event)` 在用户点击操作按钮时的回调
- **types:** `Array` 任务类型，当拖动任务卡片时，如果有的板块的状态不属于该任务的类型，则会被禁用
  - **id:** `Number,String` 类型标识
  - **states:** `Array` 状态列表
    - **id:** `Number,String` 标识
- **onLoad:** `Function(Object, Function)` 在开始加载下一页任务列表时的回调
- **onUpdate:** `Function(Object, any)` 在更新任务状态时的回调
- **onRender:** `Function(Object, JQuery)` 在渲染任务卡片时的回调
- **onSorted:** `Function(Array)` 在板块列表被排序后的回调

详细的配置参数可参考 [src/components/boards/config.js](src/components/boards/config.js) 文件，以及示例页面中的 js 代码。

## 开发

```bash
# 生成用于示例的相关资源
npm run demo

# 生成全部用于发行的资源：
npm run dist
```

## 发布

```bash
# 创建 beta 版的发行版
npm run release-beta

# 创建正式发行版
npm run release

# 发布测试版到 npm 服务器上
npm publish --tag=beta

# 发布正式版到 npm 服务器上
npm publish
```
