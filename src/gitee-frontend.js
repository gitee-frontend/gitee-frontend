import { Boards } from './components/boards/main'
import { FilteredSearchBox } from './components/filtered-search-box/main'
import VueFilteredSearchBox from './components/filtered-search-box/main.vue'

const components = [
  VueFilteredSearchBox
]

const install = function (Vue) {
  components.forEach(component => Vue.component(component.name, component))
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export { VueFilteredSearchBox, Boards, FilteredSearchBox }

export default {
  install
}
