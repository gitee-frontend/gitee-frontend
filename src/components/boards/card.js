import { Renderer } from './render'
import { htmlSafe } from './utils'

/* eslint-disable indent */

function getUserUrl(user) {
  return user.html_url || user.path
}

function renderCardUserLabel(issue) {
  if (!issue.author) {
    return ''
  }
  if (typeof issue.author.is_member !== 'undefined') {
    if (!issue.author.is_member) {
      return '<span class="user-label blue">[访客]</span>'
    }
  }
  if (issue.author.outsourced) {
    return '<span class="user-label red">[外包]</span>'
  }
  return ''
}

function renderCardLabels(issue, labelsTemplate) {
  if (!issue.labels || !labelsTemplate) {
    return ''
  }
  const html = issue.labels.map(labelsTemplate).join('')
  return `<div class="labels">${html}</div>`;
}

export class CardRenderer extends Renderer {
  render(issue) {
    const user = this.config.user
    const readonly = this.config.readonly

    let draggable = ''
    let cardClass = this.config.className.card

    if (!readonly && (user.admin || user.id === issue.author.id)) {
      draggable = 'draggable="true"'
      cardClass += ' card-draggable'
    }
    return `<li class="${cardClass}" ${draggable}
      id="${this.config.name}-issue-${issue.id}" data-id="${issue.id}">
      <div class="content">
        <a class="ui small header card-header" href="${issue.html_url}"
        title="${htmlSafe(issue.title)}" target="_blank">
          ${htmlSafe(issue.title)}
        </a>
        ${this.config.topLevelLabel && this.config.topLevelLabel(issue) || ''}
        ${this.config.priorityLabel && this.config.priorityLabel(issue) || ''}
      </div>
      <div class="extra content">
        ${this.renderAssignee(issue)}
        ${this.renderState(issue)}
        ${renderCardLabels(issue, this.config.labelsTemplate)}
        <span class="card-number">#${issue.number}</span>
        ${renderCardUserLabel(issue)}
        ${this.renderComments(issue)}
      </div>
    </li>`
  }

  renderState(issue) {
    const state = issue.state_data

    if (!state || this.config.key === 'state') {
      return ''
    }
    return `<div class="state">
      <i title="${state.name}" class="${state.icon}" style="color: ${state.color}"></i>
    </div>`
  }

  renderAssignee(issue) {
    const user = issue.assignee

    if (!user || !user.id || this.config.key !== 'state') {
      return ''
    }
    return `<div class="assignee">
        <a target="_blank" href="${getUserUrl(user)}" title="${htmlSafe(user.name)}">
          <img src="${this.getAvatarUrl(user.name, user.avatar_url)}"
            alt="${htmlSafe(user.name)}" class="${this.config.className.avatar}">
        </a>
      </div>`
  }

  renderComments(issue) {
    const className = this.config.className.iconComment

    if (!issue.comments) {
      return ''
    }
    return `<span class="card-comments"><i class="${className}"></i><span>${issue.comments}</span></span>`
  }
}

export default CardRenderer
