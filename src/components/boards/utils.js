
export const htmlSafe = (function () {
  const $el = $('<div/>')

  return function (text) {
    return $el.text(text).html();
  }
}())

export default null
