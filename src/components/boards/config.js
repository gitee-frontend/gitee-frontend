import { htmlSafe } from './utils'
export const defaults = {
  readonly: false,
  key: 'state',
  name: 'board',
  message: {
    loading: 'loading ...',
    stateDisabled: 'Current issue cannot switch to this state',
    allComplete: 'Showing all issues',
    empty: 'There are no issues here',
    btnSetFirst: 'Move it to the first',
    btnSetLast: 'Move it to the last',
  },
  className: {
    iconComment: 'icon comments outline',
    iconAngleLeft: 'icon angle double left',
    iconAngleRight: 'icon angle double right',
    iconIssue: 'icon sticky note outline',
    card: 'ui link card',
    action: 'ui button',
    actions: 'ui mini basic icon buttons',
    avatar: 'ui avatar image'
  },
  labelsTemplate: (label) => {
    return ` <span class="label" style="background-color: #${label.color}; color: #fff">
    ${htmlSafe(label.name)}
  </span>`
  },
  actions(config) {
    if (!config.plugins.Sortable) {
      return []
    }
    return [
      {
        id: 'btn-set-first',
        class: config.className.action,
        icon: config.className.iconAngleLeft,
        title: config.message.btnSetFirst,
        callback(boards, board) {
          const state = board.state.toString()
          const states = boards.sortable.toArray()
          const i = states.indexOf(state)

          if (i >= 0) {
            states.splice(i, 1)
            states.splice(0, 0, state)
            boards.sortable.sort(states)
            config.onSorted(states)
            boards.load()
          }
        }
      },
      {
        id: 'btn-set-last',
        class: config.className.action,
        icon: config.className.iconAngleRight,
        title: config.message.btnSetLast,
        callback(boards, board) {
          const state = board.state.toString()
          const states = boards.sortable.toArray()
          const i = states.indexOf(state)

          if (i >= 0) {
            states.splice(i, 1)
            states.push(state)
            boards.sortable.sort(states)
            config.onSorted(states)
            boards.load()
          }
        }
      }
    ]
  },
  data: [
    {
      order: 1,
      name: 'Backlog',
      state: 'open',
      color: '#ffa726',
      issuesCount: 0
    }, {
      order: 2,
      name: 'Done',
      state: 'closed',
      color: '#2baf2b',
      issuesCount: 0
    }
  ],
  plugins: {},
  types: [],
  user: {
    id: 0,
    admin: false
  },

  /**
   * 在任务列表加载完后的回调
   * @callback BoardLoadCallback
   * @param {Array} issues 任务列表
   * @param {Number} count 任务总数
   */

  /**
   * 在开始加载任务列表时的回调
   * @param {Object,Board} board 板块对象
   * @param {BoardLoadCallback} callback 用于接收任务列表的回调函数
   */
  onLoad(board, callback) {
    $.ajax({
      url: 'issues',
      data: {
        state: board.state,
        page: board.page
      }
    }).done(() => {
      callback([], 0)
    }).fail(() => {
      callback([], 0)
    })
  },
  /**
   * 在更新任务时的回调
   * @param {Object} issue 任务
   * @param {String,Number} oldState 更新前的状态
   */
  onUpdate(issue, oldState) {
    $.ajax({
      type: 'PUT',
      url: issue.url,
      data: {
        state: issue.state
      }
    }).done((res) => {
      this.updateIssue(res)
    }).fail(() => {
      this.setIssueState(issue.id, oldState)
    })
  },
  /**
   * 在渲染任务卡片时的回调
   * @param {Object} issue 任务
   * @param {JQuery} $el 任务卡片
   */
  onRender(issue, $el) {
    $el.addClass(`issue-state-${issue.state}`)
    return $el
  },
  /**
   * 在板块被排序后的回调
   * @param {Array} states 状态列表
   */
  onSorted(states) {
    window.console.log(states)
  }
}

export class Config {
  constructor(config) {
    $.extend(this, defaults, config)
    if (config.className) {
      this.className = $.extend({}, defaults.className, config.className)
    }
    if (config.message) {
      this.message = $.extend({}, defaults.message, config.message)
    }
  }
}

export default Config
