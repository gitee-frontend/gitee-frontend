/* eslint-disable no-plusplus */

import template from './template'
import { htmlEscape } from './util'

function render(tmpl, data) {
  let html = tmpl

  Object.keys(data).forEach(function (k) {
    html = html.replace('{' + k + '}', data[k])
  })

  return html
}

export const itemDivider = template.itemDivider

export function renderFilterDropdownTabs(tabs) {
  const html = $(template.filterDropdownTabs)
  tabs.forEach(item => html.append(render(template.filterDropdownTab, item)))
  return html
}

export function renderSearchTip(searchTip) {
  const html = render(template.filterSearchTip, {searchTip})
  return html
}

export function renderFilterDropdownItem(item) {
  var html = template.filterDropdownItem
  var keymap = {
    image: 'itemImage',
    description: 'itemDescription'
  }

  Object.keys(keymap).forEach(function (k) {
    var node = ''

    if (item[k]) {
      node = template[keymap[k]].replace('{content}', item[k])
    }
    html = html.replace('{' + k + '}', node)
  })

  if (item.icon) {
    html = render(html, {
      icon: render(template.itemIcon, {
        class: ' ' + item.icon,
        style: item.iconStyle || ''
      })
    })
  } else {
    html = render(html, { icon: '' })
  }
  return render(html, {
    name: htmlEscape(item.name),
    value: item.value,
    keywords: htmlEscape(item.keywords || ''),
    class: item.class ? ' ' + item.class : '',
    key: item.key || ''
  })
}

export function renderInputDropdownItem(item) {
  var data = {}

  data.key = item.key || 'cmd'
  data.name = item.name
  if (item.icon) {
    data.icon = '<i class="' + item.icon + '"></i>'
  } else {
    data.icon = ''
  }
  if (item.placeholder) {
    data.placeholder = ':' + item.placeholder
  } else {
    data.placeholder = ''
  }
  if (item.class) {
    data.class = ' ' + item.class
  } else {
    data.class = ''
  }
  return render(template.inputDropdownItem, data)
}

export function renderItemIcon(data) {
  if (data.icon) {
    return render(template.itemIcon, {
      style: data.iconStyle || '',
      class: ' ' + data.icon
    })
  }
  return ''
}

export function renderInputDropdownHeader(header) {
  return render(template.inputDropdownHeader, { header: header })
}

export function renderVisualSearchTerm(data) {
  return render(template.visualSearchTerm, { value: htmlEscape(data.value) })
}

export class Renderer {
  renderFilterTokenValueByItem(item) {
    var html = template.visualTokenValue
    html = render(html, {
      name: htmlEscape(item.name),
      value: item.value,
      iconClass: this.config.removeIconClass
    })
    html = render(html, { icon: renderItemIcon(item) })
    if (item.image) {
      html = render(html, {
        image: render(template.itemImage, { content: item.image })
      })
    } else {
      html = render(html, { image: '' })
    }
    if (item.color) {
      return render(html, {
        class: ' has-color',
        style: 'background-color: ' + item.color
      })
    }
    return render(html, { class: '', style: '' })
  }

  renderFilterTokenValue(filter, value) {
    var i
    var item
    var items = filter.items
    var valueStr = value.toString()

    if (filter.type === 'daterange') {
      return this.renderFilterTokenValueByItem({ name: value, value: value })
    }
    if (typeof filter.none === 'object') {
      items = [filter.none].concat(items)
    }
    for (i = 0; i < items.length; ++i) {
      item = items[i]
      if (item.value.toString() === valueStr) {
        return this.renderFilterTokenValueByItem(item)
      }
    }
    return ''
  }

  renderVisualToken(filter, value) {
    var values
    var valuesHtml = ''
    var html = template.visualToken

    if (filter.multiple) {
      values = value
    } else {
      values = [value]
    }
    html = render(template.visualToken, filter)
    if (typeof value !== 'undefined') {
      values.forEach((val) => {
        valuesHtml += this.renderFilterTokenValue(filter, val)
      })
    }
    html = html.replace('{value}', valuesHtml)
    return html
  }

  renderBody() {
    return [
      template.container,
      render(template.btnClearSearch, {
        iconClass: this.config.removeIconClass
      })
    ].join('')
  }
}
