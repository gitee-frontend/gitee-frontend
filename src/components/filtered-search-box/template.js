/* eslint-disable indent */
export default {
  container: [
    '<div class="filtered-search-box-input-container">',
      '<ul class="tokens-container">',
        '<li class="input-token ui search dropdown">',
          '<input autocomplete="off" class="search filtered-search">',
          '<div class="menu filter-items"></div>',
        '</li>',
      '</ul>',
    '</div>'
  ].join(''),
  btnClearSearch: [
    '<button class="clear-search" type="button">',
      '<i aria-hidden="true" data-hidden="true" class="{iconClass}"></i>',
    '</button>'
  ].join(''),
  itemIcon: '<i class="item-icon{class}" style="{style}"></i>',
  itemDivider: '<div class="divider"></div>',
  itemImage: '<img class="item-image ui avatar image" alt="image" src="{content}" />',
  itemDescription: '<span class="description">{content}</span>',
  inputDropdownItem: [
    '<div class="item{class}" data-value="{key}">',
      '{icon}{name}{placeholder}',
    '</div>'
  ].join(''),
  filterDropdownItem: [
    '<div class="item{class}" data-value="{value}" data-tab="{key}">',
      '{icon}{image}{name}<span class="item-keywords">{keywords}</span>{description}',
    '</div>'
  ].join(''),
  filterDropdownTabs: `
    <div class="header tabs-wrapper">
  `,
  filterDropdownTab: `
    <span class="tab {active}" data-tab="{key}">{name}</span>
  `,
  filterSearchTip: `
    <div class="item search-tips" data-tab="visitor">{searchTip}</div>
  `,
  inputDropdownHeader: '<div class="header">{header}</div>',
  visualTokenValue: [
    '<div class="value-container{class}" style="{style}">',
      '<div class="value" data-value="{value}">{icon}{image}{name}</div>',
      '<div class="remove-token inverted" role="button">',
        '<i class="{iconClass}"></i>',
      '</div>',
    '</div>'
  ].join(''),
  visualSearchTerm: [
    '<li class="js-visual-token filtered-search-term">',
      '<div class="name">{value}</div>',
    '</li>'
  ].join(''),
  visualToken: [
    '<li class="js-visual-token filtered-search-token" data-key="{key}">',
      '<div class="selectable" role="button">',
        '<div class="name">{name}</div>',
        '{value}',
      '</div>',
    '</li>'
  ].join('')
}
