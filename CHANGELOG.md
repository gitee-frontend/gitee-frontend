# [0.22.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.1...v0.22.0) (2021-06-09)


### Features

* **boards:** 添加 labelsTemplate 配置项、调整 board.color 的效果 ([10c2242](https://gitee.com/gitee-frontend/gitee-frontend/commits/10c2242e6ddadbfebb63ed79040ece877d014017))



## [0.21.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.1-beta.2...v0.21.1) (2020-07-28)



## [0.21.1-beta.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.1-beta.1...v0.21.1-beta.2) (2020-07-08)


### Bug Fixes

* **boards:** 纠正错误的自闭合标签语法 ([a7eda4c](https://gitee.com/gitee-frontend/gitee-frontend/commits/a7eda4cf2137effbb487324e7cecd137285d2447))



## [0.21.1-beta.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.1-beta.0...v0.21.1-beta.1) (2020-06-08)


### Bug Fixes

* **boards:** 直接调用 setIssueState() 时未移除卡片 loading 状态 (!11) ([b53d23a](https://gitee.com/gitee-frontend/gitee-frontend/commits/b53d23a9011443d521fbc5458a6da1eef4f82895))



## [0.21.1-beta.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.1-beta...v0.21.1-beta.0) (2020-05-29)


### Bug Fixes

* **boards:** 移除按钮中的多余单引号 ([a3cd086](https://gitee.com/gitee-frontend/gitee-frontend/commits/a3cd0862082297f3fedbe33606127bee71a11d71))


### Features

* **boards:** 更新卡片状态时增加 loading 样式 ([a1b33ad](https://gitee.com/gitee-frontend/gitee-frontend/commits/a1b33ada5f8628ce78013aeef31c6fc538f1e31b))



## [0.21.1-beta](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.0...v0.21.1-beta) (2020-01-06)



# [0.21.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.0-beta.2...v0.21.0) (2020-01-02)



# [0.21.0-beta.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.0-beta.1...v0.21.0-beta.2) (2019-12-09)


### Bug Fixes

* __vue_normalizer__ is not a function ([c562747](https://gitee.com/gitee-frontend/gitee-frontend/commits/c562747195e89d3f8d4f43b111f769b683797f71))



# [0.21.0-beta.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.0-beta.0...v0.21.0-beta.1) (2019-12-06)


### Bug Fixes

* rollup-plugin-vue 插入的代码未转换为 es5 兼容语法 ([953fb70](https://gitee.com/gitee-frontend/gitee-frontend/commits/953fb700149dff7483fd0f95a4efa99077afc2de))



# [0.21.0-beta.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.21.0-beta...v0.21.0-beta.0) (2019-12-06)


### Bug Fixes

* vue-runtime-helpers was not found ([9e8ad46](https://gitee.com/gitee-frontend/gitee-frontend/commits/9e8ad46368b86fa461da79d230fdd6d147972f5d))



# [0.21.0-beta](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.20.2...v0.21.0-beta) (2019-12-06)


### Bug Fixes

* **filtered-search-box:** 修复 data-tab 值为 undefined (!9) ([5c544b9](https://gitee.com/gitee-frontend/gitee-frontend/commits/5c544b9d6df46ad4145e4ed71298f9e3067ccb08))


### Features

* **filtered-search-box:** 添加 onHide() 回调 ([dec367c](https://gitee.com/gitee-frontend/gitee-frontend/commits/dec367c4febf9eb3f71c375d7efab9db38c2a242))



## [0.20.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.20.1...v0.20.2) (2019-10-17)


### Bug Fixes

* 纠正错误的依赖 ([6c5ea36](https://gitee.com/gitee-frontend/gitee-frontend/commits/6c5ea364bf9f0d589c4c79d0c72656eacc50fff1))



## [0.20.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.20.1-beta.1...v0.20.1) (2019-10-10)



## [0.20.1-beta.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.20.0...v0.20.1-beta.1) (2019-09-24)


### Bug Fixes

* **filtered-search-box:** 修复远程数据为空时下拉列表显示问题 (!7) ([27beec5](https://gitee.com/gitee-frontend/gitee-frontend/commits/27beec5f94e6e78e75e08cb7b9e075ac75712241))



# [0.20.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.20.0-beta.2...v0.20.0) (2019-09-17)



# [0.20.0-beta.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.20.0-beta.1...v0.20.0-beta.2) (2019-09-12)


### Bug Fixes

* **filtered-search-box:** 第一次按方向键未移动输入框的光标 ([84d785f](https://gitee.com/gitee-frontend/gitee-frontend/commits/84d785fbb204ce6bdefb7c5de7d80b86d003c287))



# [0.20.0-beta.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.19.0...v0.20.0-beta.1) (2019-09-11)


### Features

* **filtered-search-box:** 添加 generateHistoryItem() 方法 (!6) ([c083666](https://gitee.com/gitee-frontend/gitee-frontend/commits/c08366683afba3e5c31faa6cf977c59ae4a9b500))



# [0.19.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.19.0-beta.1...v0.19.0) (2019-09-09)



# [0.19.0-beta.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.18.0...v0.19.0-beta.1) (2019-08-27)


### Bug Fixes

* **filtered-search-box:** 选完筛选条件后，候选框无法关闭 (!5) ([da788ce](https://gitee.com/gitee-frontend/gitee-frontend/commits/da788ce9301ab87658c40801eb66274c910e332d))



# [0.18.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.18.0-beta.2...v0.18.0) (2019-08-23)



# [0.18.0-beta.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.18.0-beta.1...v0.18.0-beta.2) (2019-08-08)


### Bug Fixes

* **filtered-search-box:** 菜单项重复 (!4) ([107cea7](https://gitee.com/gitee-frontend/gitee-frontend/commits/107cea796f115dc8e4285af0de537b24cd3ed986))



# [0.18.0-beta.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.17.1...v0.18.0-beta.1) (2019-08-08)


### Features

* **filtered-search-box:** 支持远程搜索 (!3) ([7a2796e](https://gitee.com/gitee-frontend/gitee-frontend/commits/7a2796eb53883bcf336eee3610549ca9fc599e14))



## [0.17.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.17.0...v0.17.1) (2019-07-01)


### Bug Fixes

* **boards:** 板块内的任务变化后应该更新底部的提示信息 ([af5956f](https://gitee.com/gitee-frontend/gitee-frontend/commits/af5956f0c1155ae50a79751b5c38cb23d1813b2c))



# [0.17.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.16.0...v0.17.0) (2019-07-01)


### Bug Fixes

* **boards:** FireFox 下不能正常拖动卡片 ([ff80d66](https://gitee.com/gitee-frontend/gitee-frontend/commits/ff80d6655d1f12fcaeb9f65b2ec085afe3c9af44))



# [0.16.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.15.3...v0.16.0) (2019-04-15)


### Features

* **boards:** onRender() 方法追加传入 config 参数 ([7890125](https://gitee.com/gitee-frontend/gitee-frontend/commits/7890125d630bfd10f7b2cf35f53de4d1e2028af6))



## [0.15.3](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.15.2...v0.15.3) (2019-03-29)


### Bug Fixes

* **boards:** 应根据配置中的 key 值来获取板块，而不是 state ([ad8a989](https://gitee.com/gitee-frontend/gitee-frontend/commits/ad8a9890c7c9563eab9012127ee6a2d6923a545a))



## [0.15.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.15.1...v0.15.2) (2019-02-26)


### Bug Fixes

* **boards:** 无任务时应该显示提示 ([457e1f9](https://gitee.com/gitee-frontend/gitee-frontend/commits/457e1f9edb1c8ced74fcc9414e07895e459a5608))



## [0.15.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.15.0...v0.15.1) (2018-12-07)


### Bug Fixes

* **filtered-search-box:** 输入关键词并按回车会使下拉菜单闪动 ([c66f6bb](https://gitee.com/gitee-frontend/gitee-frontend/commits/c66f6bb8759db374f9b7c9e4087607c692299757))



# [0.15.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.14.2...v0.15.0) (2018-12-04)


### Features

* **filtered-search-box:** 添加 blur 事件 ([f88c1a4](https://gitee.com/gitee-frontend/gitee-frontend/commits/f88c1a4fe3790797fe70c0f1c8a40cdb01d660f0))



## [0.14.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.14.1...v0.14.2) (2018-11-22)


### Bug Fixes

* **filtered-search-box:** 点击下拉框的滚动条时不应该收起下拉框 ([05eee40](https://gitee.com/gitee-frontend/gitee-frontend/commits/05eee402d455aef5ab4e68a986a51dac28f92127))



## [0.14.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.14.0...v0.14.1) (2018-11-16)


### Bug Fixes

* **boards:** 只读模式下应禁止拖动卡片 ([8af9f6d](https://gitee.com/gitee-frontend/gitee-frontend/commits/8af9f6d96aa2b5af733c8550290828ede1be58a4))
* **boards:** 板块里没有任务总数时应显示 0 ([5134523](https://gitee.com/gitee-frontend/gitee-frontend/commits/5134523836e45370c9d56953ff7e4de8e6e610ee))



# [0.14.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.13.0...v0.14.0) (2018-11-14)


### Bug Fixes

* **boards:** 更新和移除板块内的任务时出错 ([3958f8f](https://gitee.com/gitee-frontend/gitee-frontend/commits/3958f8f6f669ab119704e4c18f0893e99df5469e))


### Features

* 添加 Vue 版插件接口 ([2dc083e](https://gitee.com/gitee-frontend/gitee-frontend/commits/2dc083e5f0ee3cfc961ee9f9c41ecf9c77f0f42a))



# [0.13.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.12.1...v0.13.0) (2018-11-13)


### Bug Fixes

* **filtered-search-box:** 生成条件组名称时应该排除空条件 ([e356407](https://gitee.com/gitee-frontend/gitee-frontend/commits/e3564071a34446a7904289eb070802149477d012))


### Features

* **filtered-search-box:** 调整 change 和 submit 事件携带的参数 ([03cb4ea](https://gitee.com/gitee-frontend/gitee-frontend/commits/03cb4eabe06f395c1c4a4589ce3a3189f7f95dc9))



## [0.12.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.12.0...v0.12.1) (2018-11-13)


### Bug Fixes

* **filtered-search-box:** 不应该保存空的搜索记录 ([a1599db](https://gitee.com/gitee-frontend/gitee-frontend/commits/a1599dba3b5cc7ec0af871ae7e38a42375de96da))



# [0.12.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.11.1...v0.12.0) (2018-11-13)


### Features

* **filtered-search-box:** 支持保存历史搜索记录 ([ca98060](https://gitee.com/gitee-frontend/gitee-frontend/commits/ca98060bd5b861d6b02f97c75c4d3e9426c6afc0))



## [0.11.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.11.0...v0.11.1) (2018-11-12)


### Bug Fixes

* **boards:** 板块排序后不会调用 onSorted() ([779ad51](https://gitee.com/gitee-frontend/gitee-frontend/commits/779ad51465ce87833c1b04876fd7e074a3775b6e))



# [0.11.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.10.3...v0.11.0) (2018-11-09)


### Features

* **boards:** 支持将板块移动到首位和末位 ([a0b9b5f](https://gitee.com/gitee-frontend/gitee-frontend/commits/a0b9b5fae939ed74f3077df800add28d69d22cf1))



## [0.10.3](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.10.2...v0.10.3) (2018-11-09)


### Bug Fixes

* **filtered-search-box:** 点击删除条件时，不应取消输入框的焦点 ([03da8c7](https://gitee.com/gitee-frontend/gitee-frontend/commits/03da8c7a4a95ff0a56321e11f25f54234616828c))



## [0.10.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.10.1...v0.10.2) (2018-11-09)


### Bug Fixes

* **filtered-search-box:** submit 事件携带的数据不完整 ([4f49851](https://gitee.com/gitee-frontend/gitee-frontend/commits/4f498515aba408c3e5bf382cd760c6c3105ab950))



## [0.10.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.10.0...v0.10.1) (2018-10-31)


### Bug Fixes

* **boards:** 创建 Boards 对象时不应自动加载数据 ([9b70f42](https://gitee.com/gitee-frontend/gitee-frontend/commits/9b70f42a54207f207033d6773c25e74235a44951))
* **boards:** 清空板块后未重置状态 ([942332a](https://gitee.com/gitee-frontend/gitee-frontend/commits/942332a6fec6215b4ffa367c0012704ed23b2955))



# [0.10.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.9.1...v0.10.0) (2018-10-31)


### Features

* 添加看板组件 ([e998f34](https://gitee.com/gitee-frontend/gitee-frontend/commits/e998f3469c3144cac02e1c63cd67a6a96b633d67))



## [0.9.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.9.0...v0.9.1) (2018-10-23)


### Bug Fixes

* **markdown:** 标题左侧的图钉位置不正确 ([303ca8c](https://gitee.com/gitee-frontend/gitee-frontend/commits/303ca8c8808516e31b59b9d082dc03e8f54e74e2))



# [0.9.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.8.2...v0.9.0) (2018-09-18)


### Features

* **filtered-search-box:** 添加 groups 参数，用于配置筛选器分组 ([698a6dc](https://gitee.com/gitee-frontend/gitee-frontend/commits/698a6dc8110f32d951a7327c62ffa5389a275477))



## [0.8.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.8.1...v0.8.2) (2018-09-17)


### Bug Fixes

* **filtered-search-box:** 删除筛选器后，input 事件传递的筛选条件不应为 undefined ([d4be428](https://gitee.com/gitee-frontend/gitee-frontend/commits/d4be428bc605bfe2b41f141b5ab6aebb0bc58bb5))
* **filtered-search-box:** 连续多次调用 setData() 后，会出现重复的筛选器 ([c7fb2a6](https://gitee.com/gitee-frontend/gitee-frontend/commits/c7fb2a64b81107776ac0a6f1fbc73fbd2c94850e))



## [0.8.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.8.0...v0.8.1) (2018-09-13)


### Performance Improvements

* **markdown:** remove "a:hover" underline ([23266f4](https://gitee.com/gitee-frontend/gitee-frontend/commits/23266f45a599463a0049430b652244892e754d76))



# [0.8.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.7.2...v0.8.0) (2018-09-13)


### Features

* add markdown css ([019eb2e](https://gitee.com/gitee-frontend/gitee-frontend/commits/019eb2e0567f9cf440d433fa2b10a9750e60c945))



## [0.7.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.7.1...v0.7.2) (2018-09-03)


### Bug Fixes

* **filtered-search-box:** 选择其它筛选条件时不应该隐藏下拉菜单 ([e76021e](https://gitee.com/gitee-frontend/gitee-frontend/commits/e76021ec5aee9a580e5f1e7587b80ff414ab24e6))



## [0.7.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.7.0...v0.7.1) (2018-08-30)


### Performance Improvements

* **filtered-search-box:** 退格键删除关键词时，切换为编辑关键词 ([9b1198f](https://gitee.com/gitee-frontend/gitee-frontend/commits/9b1198f8631db499babf7ce86620971e5bc75027))



# [0.7.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.6.0...v0.7.0) (2018-08-28)


### Bug Fixes

* **filtered-search-box:** 取消编辑筛选条件后，未重置输入框到末尾 ([f8f2c22](https://gitee.com/gitee-frontend/gitee-frontend/commits/f8f2c22aa15d30ad0bc4dca699e9dd3840dd5f56))
* **filtered-search-box:** 用按键操作新建筛选条件后，输入框内容未清空 ([3430a99](https://gitee.com/gitee-frontend/gitee-frontend/commits/3430a99ce6adf229b41baad6f12bfbd2cc5d2ae9))
* **filtered-search-box:** 编辑“无”值时，输入框应该填充“无”值的名称 ([b0b61b2](https://gitee.com/gitee-frontend/gitee-frontend/commits/b0b61b2ade076496f98d7ca966f3c54ebf1c4464))


### Performance Improvements

* **filtered-search-box:** 用退格键删除条件时，改为删除整块条件 ([94415ec](https://gitee.com/gitee-frontend/gitee-frontend/commits/94415ec1d01e50c0c2e392eb3e844c5dd4cf7ea3))



# [0.6.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.5.4...v0.6.0) (2018-08-23)


### Bug Fixes

* **filtered-search-box:** 编辑多选的筛选器时，会选中重复的条件 ([cb2ab1f](https://gitee.com/gitee-frontend/gitee-frontend/commits/cb2ab1fd19936b99245bbd1c097df8098bee4340))
* **filtered-search-box:** 选项列表中的“无”值不应该被过滤掉 ([1b43ce1](https://gitee.com/gitee-frontend/gitee-frontend/commits/1b43ce16064d7a17fdbe7d2346889f9310343475))


### Features

* **filtered-search-box:** 添加 items.keywords 配置，用于为筛选选项设置关键词 ([629295e](https://gitee.com/gitee-frontend/gitee-frontend/commits/629295ef878f61c832cb1d460b52b2591127ebf0))



## [0.5.4](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.5.3...v0.5.4) (2018-08-22)


### Bug Fixes

* **filtered-search-box:** 当筛选器有“无”值时，即使没有可选项也应该可见 ([41c4555](https://gitee.com/gitee-frontend/gitee-frontend/commits/41c45551e8c6e43d23cc167137a703cff9e69bb9))



## [0.5.3](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.5.2...v0.5.3) (2018-08-22)


### Bug Fixes

* **filter-search-box:** 未对输入的内容进行转义 ([fdaeb80](https://gitee.com/gitee-frontend/gitee-frontend/commits/fdaeb80b8c7662e40f52a0c61daebef5b35b5f7a))



## [0.5.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.5.1...v0.5.2) (2018-08-22)


### Bug Fixes

* **filtered-search-box:** 筛选条件太长时内容会溢出 ([73b77fe](https://gitee.com/gitee-frontend/gitee-frontend/commits/73b77fe9e6c2911c36151d95cbff9c51365b2529))



## [0.5.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.5.0...v0.5.1) (2018-08-21)


### Bug Fixes

* **filtered-search-box:** 当筛选器有“无”值时，应判定为可见 ([db981b9](https://gitee.com/gitee-frontend/gitee-frontend/commits/db981b9c585d8cf7cd07387e4cdcdb881c15e374))
* **filtered-search-box:** 点选筛选条件时下拉菜单会反复展开/收缩 ([033734d](https://gitee.com/gitee-frontend/gitee-frontend/commits/033734d5da4eb19a7570228d791a8bb00eefa2c6))


### Features

* **filtered-search-box:** 添加筛选器时，如果已经存在则直接编辑它 ([1603f87](https://gitee.com/gitee-frontend/gitee-frontend/commits/1603f87b77e9eb8eb01c5877b3e74d38f28f8e96))



# [0.5.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.4.6...v0.5.0) (2018-08-20)


### Features

* **filtered-search-box:** 为 Vue 版组件添加 submit 事件 ([d3084ed](https://gitee.com/gitee-frontend/gitee-frontend/commits/d3084ed752989a0fcd6d2cd5b692a6e0bf3d219e))


### Performance Improvements

* **filtered-search-box:** 将 number 转换为字符串 ([f6e38cd](https://gitee.com/gitee-frontend/gitee-frontend/commits/f6e38cd5547654434856c89be36e4911cbda6a24))



## [0.4.6](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.4.5...v0.4.6) (2018-08-17)


### Bug Fixes

* **filtered-search-box:** Vue 组件版在修改筛选后，有时不会触发 input 事件 ([e8b4fb4](https://gitee.com/gitee-frontend/gitee-frontend/commits/e8b4fb413f4710f2a79074ce7cd28bf15556fb32))



## [0.4.5](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.4.4...v0.4.5) (2018-08-17)


### Bug Fixes

* **filtered-search-box:** 检测数据更新时，并未检测 search 字段的差异 ([198192a](https://gitee.com/gitee-frontend/gitee-frontend/commits/198192a3f8470e423ce81128902ec997fbf3b9d5))



## [0.4.4](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.4.3...v0.4.4) (2018-08-16)


### Bug Fixes

* **filtered-search-box:** 输入筛选器名称后按回车键并未新建筛选 ([667bf6b](https://gitee.com/gitee-frontend/gitee-frontend/commits/667bf6b286717b830c46c949f6ec8469c7e2e3fb))
* **filtered-search-box:** 选择选项后，选项值类型未转换为 String ([e1ea5cf](https://gitee.com/gitee-frontend/gitee-frontend/commits/e1ea5cfedbe1729a852d47b8341bd71ddbb7d801))


### Performance Improvements

* **filtered-search-box:** 调整 Vue 组件版的数据更新检测方式 ([442d258](https://gitee.com/gitee-frontend/gitee-frontend/commits/442d25839475ea78b8a709372ecdfff542f44085))



## [0.4.3](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.4.2...v0.4.3) (2018-08-14)


### Bug Fixes

* **filtered-search-box:** 手动输入条件后，不应将输入的内容作为搜索词 ([f8d887d](https://gitee.com/gitee-frontend/gitee-frontend/commits/f8d887d04291e65554109f752258a07926ec49ab))



## [0.4.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.4.1...v0.4.2) (2018-08-14)


### Bug Fixes

* **filtered-search-box:** that.logger.log is not a function ([228afc0](https://gitee.com/gitee-frontend/gitee-frontend/commits/228afc0c69f42f10023f508a36f678c20bd1a897))



## [0.4.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.4.0...v0.4.1) (2018-08-13)


### Bug Fixes

* **filtered-search-box:** this.popFilterValue is not a function ([fb0bcbe](https://gitee.com/gitee-frontend/gitee-frontend/commits/fb0bcbed8616804059e6a6641106ca15bf4dd6b3))
* **filtered-search-box:** this.updateSearchTerm is not a function ([22a2baf](https://gitee.com/gitee-frontend/gitee-frontend/commits/22a2bafc4e83912a3a6acc1f9971f543d4a42cc4))
* **filtered-search-box:** 事件处理报错 ([296330a](https://gitee.com/gitee-frontend/gitee-frontend/commits/296330af3ad7babe879788c341da6a99956ca919))



# [0.4.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.3.1...v0.4.0) (2018-08-13)



## [0.3.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.3.0...v0.3.1) (2018-08-10)


### Bug Fixes

* **filtered-search-box:**  this.data 会被回调函数篡改 ([2bf5778](https://gitee.com/gitee-frontend/gitee-frontend/commits/2bf5778310e239aa2bdff9048e6c2cad09584cbe))


### Performance Improvements

* **filtered-search-box:** 载入完筛选条件后，更新搜索词的位置 ([98f1f2c](https://gitee.com/gitee-frontend/gitee-frontend/commits/98f1f2cea50969702c44ce79043544c1a3fb2b8d))



# [0.3.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.2.0...v0.3.0) (2018-08-10)


### Features

* **filtered-search-box:** 添加 removeIconClass，用于自定义删除图标样式 ([e079678](https://gitee.com/gitee-frontend/gitee-frontend/commits/e079678a48c9867676c2870505dadfcb6bc3a01c))



# [0.2.0](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.1.4...v0.2.0) (2018-08-09)


### Bug Fixes

* **filtered-search-box:** item.iconStyle 对已选中的选项没有效果 ([fcb6300](https://gitee.com/gitee-frontend/gitee-frontend/commits/fcb630031049b5ae0533ea2cfd551fae6ac03f85))
* **filtered-search-box:** 新建时间范围筛选时出现 undefined 内容 ([2b7c339](https://gitee.com/gitee-frontend/gitee-frontend/commits/2b7c339b9776e990ba4013625965c976a34009f5))


### Features

* **filtered-search-box:** 添加支持编辑搜索词 ([2a128d0](https://gitee.com/gitee-frontend/gitee-frontend/commits/2a128d063933f719150c70e4b0bee330f6daa32a))



## [0.1.4](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.1.3...v0.1.4) (2018-08-07)


### Bug Fixes

* **filtered-search-box:** 移除筛选条件后，数据并未改变 ([d21bc6c](https://gitee.com/gitee-frontend/gitee-frontend/commits/d21bc6c6beec0eaf655b38d20be45fbfa867b3a1))
* **filtered-search-box:** 移除筛选条件后，未更新下拉菜单内容 ([c14cf0c](https://gitee.com/gitee-frontend/gitee-frontend/commits/c14cf0ce6dd86089301c603387519588eb9fbe7d))



## [0.1.3](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.1.2...v0.1.3) (2018-08-07)


### Bug Fixes

* **filtered-search-box:** 重设筛选器列表后，在读取选项列表时会报错 ([634d30e](https://gitee.com/gitee-frontend/gitee-frontend/commits/634d30e041cac6f4f8877b2c169e3435a16a26dc))


### Performance Improvements

* **filtered-search-box:** 改进空值判断 ([de03d53](https://gitee.com/gitee-frontend/gitee-frontend/commits/de03d539c78d11cc6a4c25ee634be0d3d14dd06b))



## [0.1.2](https://gitee.com/gitee-frontend/gitee-frontend/compare/v0.1.1...v0.1.2) (2018-08-06)


### Bug Fixes

* **filtered-search-box:** 取消编辑时间范围筛选器后，未还原正确的值 ([734ac3e](https://gitee.com/gitee-frontend/gitee-frontend/commits/734ac3ed58f754ca1f87d2c612038eb6e1ac4783))
* **filtered-search-box:** 编辑筛选器时，会清空原有内容 ([dbb2b3c](https://gitee.com/gitee-frontend/gitee-frontend/commits/dbb2b3c37a82f999939df7357690197b299e3a27))



## [0.1.1](https://gitee.com/gitee-frontend/gitee-frontend/compare/ebf5d2347f46f2772024343d37072f09dcb608bd...v0.1.1) (2018-08-06)


### Bug Fixes

* **filtered-search-box:** text.placeholder 没有默认值 ([0b87d25](https://gitee.com/gitee-frontend/gitee-frontend/commits/0b87d25e0f6a73629dfa3cdfff7a5e32736233be))
* **filtered-search-box:** 下拉框中的操作选项会和普通选项一起被隐藏 ([bc3d90c](https://gitee.com/gitee-frontend/gitee-frontend/commits/bc3d90cc3bc4cfd910b96b42aa4c78623359f698))
* **filtered-search-box:** 不应该显示空的筛选器 ([aaf1f04](https://gitee.com/gitee-frontend/gitee-frontend/commits/aaf1f04fa6907e9d01526aeb973d7dc0b2014a8f))
* **filtered-search-box:** 当依赖的筛选器为“无”值时，应判断为不满足依赖 ([f2414ca](https://gitee.com/gitee-frontend/gitee-frontend/commits/f2414ca2c3dacbf8b47fbbc1b0783253eefed3bd))
* **filtered-search-box:** 新建筛选条件时点击已选好的选项，不应该隐藏下拉框 ([0ef6ddc](https://gitee.com/gitee-frontend/gitee-frontend/commits/0ef6ddc7550c5ec2522ee518e3c72e5b58efdd2d))
* **filtered-search-box:** 未正确过滤掉已选中的选项 ([c260f9d](https://gitee.com/gitee-frontend/gitee-frontend/commits/c260f9d846fd82a1c8481a0172f15aaabf9a7ee6))
* **filtered-search-box:** 点击删除条件选项后，不应编辑该筛选器 ([9e2eed8](https://gitee.com/gitee-frontend/gitee-frontend/commits/9e2eed81d7033ab8bd12a5d0c0b35bc932bc79d1))
* **filtered-search-box:** 点击搜索框里未让输入框获得焦点 ([3ff5b6c](https://gitee.com/gitee-frontend/gitee-frontend/commits/3ff5b6cc65d45dbe4c908470d3e0684bdc6ef0ff))
* **filtered-search-box:** 筛选器多了后内容会溢出搜索框 ([9e052b8](https://gitee.com/gitee-frontend/gitee-frontend/commits/9e052b8b850ef115bb1f47cea54038b892c6ac6f))
* **filtered-search-box:** 编辑可多选的筛选器时没有显示“返回”选项 ([ebf5d23](https://gitee.com/gitee-frontend/gitee-frontend/commits/ebf5d2347f46f2772024343d37072f09dcb608bd))
* **filtered-search-box:** 编辑多选的筛选器时，选择一个选项后不应该退出编辑 ([1874cf1](https://gitee.com/gitee-frontend/gitee-frontend/commits/1874cf14d94ba859e29323ddf9e1a61e8e394faf))
* **filtered-search-box:** 缺少 text.loading 默认值 ([47591cb](https://gitee.com/gitee-frontend/gitee-frontend/commits/47591cb61327d993a5d532c88e87f5ad98c14c0b))
* **filtered-search-box:** 输入筛选器名后按冒号键，没有切换到筛选器的选项编辑模式 ([f1d37dc](https://gitee.com/gitee-frontend/gitee-frontend/commits/f1d37dcea65aa8f2b123a15b35cbbf207a60bc72))


### Features

* **filtered-search-box:** 添加 destroy() 方法 ([243c3da](https://gitee.com/gitee-frontend/gitee-frontend/commits/243c3dab42504ee8edc9770be98a4f7356e6cc12))
* 添加 FilteredSearchBox.vue ([707e9c8](https://gitee.com/gitee-frontend/gitee-frontend/commits/707e9c87db99ca2763cfcb70394039be8b6b3265))
* **demo:** 添加示例页面 ([f6aab8d](https://gitee.com/gitee-frontend/gitee-frontend/commits/f6aab8da681fcbd6d19db8a1be049f3c6c986555))
* **demo:** 添加筛选搜索框的示例代码 ([4131605](https://gitee.com/gitee-frontend/gitee-frontend/commits/4131605dc0814f5ecb1d7c44467b87747b961e0f))
* 添加 gitee-frontend.scss ([a24c291](https://gitee.com/gitee-frontend/gitee-frontend/commits/a24c2913c5aa22f1cc591d53518deebe647282a4))
* **filtered-search-box:** 删除筛选器时，其它依赖它的筛选器也会被删除 ([4453d0d](https://gitee.com/gitee-frontend/gitee-frontend/commits/4453d0d6ea61e9c179455b2497be7c00fa3564f0))
* **filtered-search-box:** 已选择的筛选条件支持显示图标、图片和背景色 ([3c2e86c](https://gitee.com/gitee-frontend/gitee-frontend/commits/3c2e86c817ee65c938e1945b9f8e03137408a070))
* **filtered-search-box:** 支持在初始化时加载远程选项数据 ([8d2f0b6](https://gitee.com/gitee-frontend/gitee-frontend/commits/8d2f0b62a9f9c69b2155c12e107cd406d9261c03))
* **filtered-search-box:** 添加 change 事件 ([c79a96c](https://gitee.com/gitee-frontend/gitee-frontend/commits/c79a96c00e04379f43b7f0bc152a1bd25bea2a66))
* **filtered-search-box:** 添加 filter.requires 参数，用于设置依赖 ([8a74e59](https://gitee.com/gitee-frontend/gitee-frontend/commits/8a74e59e09c41eefe535c80e559d19bb3cf9344d))
* **filtered-search-box:** 添加 setData() 方法 ([de8cf20](https://gitee.com/gitee-frontend/gitee-frontend/commits/de8cf20f210d2b08eed1f4133b1ae724762ca11d))
* **filtered-search-box:** 添加支持 filter.remote.fetcher 参数，用于自定义数据获取方式 ([79e5d30](https://gitee.com/gitee-frontend/gitee-frontend/commits/79e5d30b9ad0e253d0ee368cc7bf6eba1cd59152))
* **filtered-search-box:** 添加支持为筛选器设置“无”选项 ([a36041a](https://gitee.com/gitee-frontend/gitee-frontend/commits/a36041a31bdad75770263002e19e397858c22bd7))
* **filtered-search-box:** 添加支持加载远程数据 ([c69f41a](https://gitee.com/gitee-frontend/gitee-frontend/commits/c69f41add7015df37ee202272acf99b579a28be7))
* **filtered-search-box:** 添加支持时间范围筛选器 ([fabefe6](https://gitee.com/gitee-frontend/gitee-frontend/commits/fabefe6c0921df75f2bd83b939199a0f8a7a7f64))
* **filtered-search-box:** 添加支持显示占位符 ([4fe9cd4](https://gitee.com/gitee-frontend/gitee-frontend/commits/4fe9cd49dfea8321e44cf50f9f55b15dfbcb8798))
* **filtered-search-box:** 添加清空按钮 ([79d957b](https://gitee.com/gitee-frontend/gitee-frontend/commits/79d957ba03fe9aa1d450f6070ca69114c8d7324e))
* **fitlered-search-box:** 添加 setFilters() 方法 ([699ca4d](https://gitee.com/gitee-frontend/gitee-frontend/commits/699ca4d2835bc7bba5bd045da74e57a15ba1b06a))
* **npm:** 添加示例页面的资源生成脚本 ([a783526](https://gitee.com/gitee-frontend/gitee-frontend/commits/a7835260bfd5b02d0229b9dd86d6caf8ae7271e7))


### Performance Improvements

* **demo:** 加上 .ui.container，调整内容间距 ([ce8a5a6](https://gitee.com/gitee-frontend/gitee-frontend/commits/ce8a5a655254eeaadc393aa8cbda34456b45b102))
* **demo:** 补充示例代码 ([3b7c429](https://gitee.com/gitee-frontend/gitee-frontend/commits/3b7c4298a53638a4cd2910145973f0610cca824a))
* **filtered-search-box:** 调整高度 ([ff9d29f](https://gitee.com/gitee-frontend/gitee-frontend/commits/ff9d29fcee513602f80390527e6dc6c71b9ef71c))



